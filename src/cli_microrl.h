#ifndef CLI_MICRORL_H
#define CLI_MICRORL_H
/* Execute callback */
int cli_execute(int argc, const char *const *argv);
typedef struct card {
    uint8_t *uid;
    size_t size;
    char *name;
    struct card *next;
} card;
#endif /* CLI_MICRORL_H */
