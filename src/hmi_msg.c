#include <avr/pgmspace.h>
#include "hmi_msg.h"

const char NAME[] = "Henri Annilo";
const char CRLF[] = "\r\n";

static const char STR_ZERO[] PROGMEM = "zero";
static const char STR_ONE[] PROGMEM = "one";
static const char STR_TWO[] PROGMEM = "two";
static const char STR_THREE[] PROGMEM = "three";
static const char STR_FOUR[] PROGMEM = "four";
static const char STR_FIVE[] PROGMEM = "five";
static const char STR_SIX[] PROGMEM = "six";
static const char STR_SEVEN[] PROGMEM = "seven";
static const char STR_EIGHT[] PROGMEM = "eight";
static const char STR_NINE[] PROGMEM = "nine";

PGM_P const NUMBERS[] PROGMEM = {
    STR_ZERO,
    STR_ONE,
    STR_TWO,
    STR_THREE,
    STR_FOUR,
    STR_FIVE,
    STR_SIX,
    STR_SEVEN,
    STR_EIGHT,
    STR_NINE
};
