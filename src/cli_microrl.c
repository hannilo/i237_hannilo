//TODO There are most likely unnecessary includes. Clean up during lab6
#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <avr/pgmspace.h>

#include "../lib/hd44780_111/hd44780.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"

#include "hmi_msg.h"
#include "cli_microrl.h"
#include "print_helper.h"

#define NUM_ELEMS(x)        (sizeof(x) / sizeof((x)[0]))

void cli_print_help(const char *const *argv);
void cli_example(const char *const *argv);
void cli_print_ver(const char *const *argv);
void cli_print_ascii_tbls(const char *const *argv);
void cli_handle_number(const char *const *argv);
void cli_rfid_read(const char *const *argv);
void cli_add_card(const char *const *argv);
void cli_print_cards(const char *const *argv);
void cli_remove_card(const char *const *argv);
void cli_mem_stat(const char *const *argv);
void cli_print_cmd_error(void);
void cli_print_cmd_arg_error(void);


typedef struct cli_cmd {
    PGM_P cmd;
    PGM_P help;
    void (*func_p)();
    const uint8_t func_argc;
} cli_cmd_t;

card *head;

const char help_cmd[] PROGMEM = "help";
const char help_help[] PROGMEM = "Get help";
const char example_cmd[] PROGMEM = "example";
const char example_help[] PROGMEM =
    "Prints out all provided 3 arguments Usage: example <argument> <argument> <argument>";
const char ver_cmd[] PROGMEM = "version";
const char ver_help[] PROGMEM = "Print FW version";
const char ascii_cmd[] PROGMEM = "ascii";
const char ascii_help[] PROGMEM = "Print ASCII tables";
const char number_cmd[] PROGMEM = "number";
const char number_help[] PROGMEM =
    "Print and display matching number. Usage: number <decimal number>";

const char read_cmd[] PROGMEM = "read";
const char read_help[] PROGMEM = "Read Mifare card and print card ID";
const char add_cmd[] PROGMEM = "add";
const char add_help[] PROGMEM =
    "Add Mifare card. Usage: add <card uid in HEX> <card holder name>";
const char print_cmd[] PROGMEM = "print";
const char print_help[] PROGMEM = "Print stored access card list";
const char remove_cmd[] PROGMEM = "remove";
const char remove_help[] PROGMEM =
    "Remove a stored card. Usage: remove <card uid in HEX>";
const char mem_stat_cmd[] PROGMEM = "mem";
const char mem_stat_help[] PROGMEM =
    "Print memory usage and change compared to previous call";



const cli_cmd_t cli_cmds[] = {
    {help_cmd, help_help, cli_print_help, 0},
    {ver_cmd, ver_help, cli_print_ver, 0},
    {example_cmd, example_help, cli_example, 3},
    {ascii_cmd, ascii_help, cli_print_ascii_tbls, 0},
    {number_cmd, number_help, cli_handle_number, 1},
    {read_cmd, read_help, cli_rfid_read, 0},
    {add_cmd, add_help, cli_add_card, 2},
    {print_cmd, print_help, cli_print_cards, 0},
    {remove_cmd, remove_help, cli_remove_card, 1},
    {mem_stat_cmd, mem_stat_help, cli_mem_stat, 0},
};


void cli_print_help(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR("Implemented commands:\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        uart0_puts_p(cli_cmds[i].cmd);
        uart0_puts_p(PSTR(" : "));
        uart0_puts_p(cli_cmds[i].help);
        uart0_puts_p(PSTR("\r\n"));
    }
}


void cli_example(const char *const *argv)
{
    uart0_puts_p(PSTR("Command had following arguments:\r\n"));

    for (uint8_t i = 1; i < 4; i++) {
        uart0_puts(argv[i]);
        uart0_puts_p(PSTR("\r\n"));
    }
}


void cli_print_ver(const char *const *argv)
{
    (void) argv;
    uart0_puts_p(PSTR(VER_FW));
    uart0_puts_p(PSTR(VER_LIBC));
}


void cli_print_ascii_tbls(const char *const *argv)
{
    (void) argv;
    print_ascii_tbl();
    unsigned char ascii[128];

    for (unsigned char c = 0; c < 128; c++) {
        ascii[c] = c;
    }

    print_for_human(ascii, 128);
}


void cli_handle_number(const char *const *argv)
{
    int input = atoi(argv[1]);;

    for (size_t i = 0; i < strlen(argv[1]); i++) {
        if (!isdigit(argv[1][i])) {
            uart0_puts_p(PSTR("Argument is not a decimal number!\r\n"));
            return;
        }
    }

    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
    lcd_goto(LCD_ROW_2_START);

    if (input >= 0 && input < 10) {
        uart0_puts_p(PSTR("You entered the number "));
        uart0_puts_p((PGM_P)pgm_read_word(&(NUMBERS[input])));
        uart0_puts_p(PSTR(".\r\n"));
        lcd_puts_P((PGM_P)pgm_read_word(&(NUMBERS[input])));
    } else {
        uart0_puts_p(PSTR("Please enter a number between 0 and 9\r\n"));
        lcd_puts_P(PSTR("Out of bounds!"));
    }
}


void cli_rfid_read(const char *const *argv)
{
    (void) argv;
    Uid uid;
    Uid *uid_ptr = &uid;
    char buffer[3] = {0x00};

    if (PICC_IsNewCardPresent()) {
        uart0_puts_p(PSTR("Card selected."));
        uart0_puts(CRLF);
        PICC_ReadCardSerial(uid_ptr);
        uart0_puts_p(PSTR("Card type: "));
        uart0_puts(PICC_GetTypeName(PICC_GetType(uid.sak)));
        uart0_puts(CRLF);
        uart0_puts_p(PSTR("Card UID: "));

        for (byte i = 0; i < uid.size; i++) {
            print_byte_as_ascii_hex(uid.uidByte[i]);
        }

        uart0_puts_p(PSTR(" (size "));
        itoa(uid.size, buffer, 10);
        uart0_puts(buffer);
        uart0_puts_p(PSTR(" bytes)"));
        uart0_puts(CRLF);
        uart0_puts_p(PSTR("UID sak: "));
        buffer[0] = '\0';
        itoa(uid.sak, buffer, 10);
        uart0_puts(buffer);
        uart0_puts(CRLF);
    } else {
        uart0_puts_p(PSTR("Cannot select card!"));
        uart0_puts(CRLF);
    }
}


void cli_add_card(const char *const *argv)
{
    size_t target_size = strlen(argv[1]) / 2;
    uint8_t *target_uid = (uint8_t *)malloc(target_size * sizeof(uint8_t));

    if (target_uid == NULL) {
        uart0_puts_p(PSTR("fail: target_uid"));
        return;
    }

    tallymarker_hextobin(argv[1], target_uid, target_size);

    //verify card UID size
    //The Mifare Card Serial Number is a unique identifier defined in ISO 14443-3A.
    //There are 3 types of UID defined in the standard:
    //Single (4 byte), Double (7 byte) and Triple (10 bytes)
    if (target_size != 4 && target_size != 7 && target_size != 10) {
        uart0_puts_p(PSTR("Card UID must be 4, 7 or 10 bytes."));
        uart0_puts(CRLF);
        return;
    }

    //check for existing card
    card *cursor = head;

    while (cursor != NULL) {
        //size comparison
        if (cursor->size == target_size) {
            //uid comparison
            int match = memcmp(cursor->uid, target_uid, target_size);

            if (match == 0) {
                uart0_puts_p(PSTR("Can not add card. UID already in list."));
                uart0_puts(CRLF);
                return;
            }
        }

        cursor = cursor->next;
    }

    uart0_puts_p(PSTR("Now adding card."));
    uart0_puts(CRLF);
    card *new_card;
    new_card = (card *)malloc(sizeof(card));

    if (new_card == NULL) {
        uart0_puts_p(PSTR("fail: new_card"));
        return;
    }

    new_card->size = strlen(argv[1]) / 2;
    new_card->uid = target_uid;
    //add room for null terminator
    //not having this causes printing names to run over the boundary
    size_t string_length = strlen(argv[2]) + 1;
    new_card->name = malloc(sizeof(char) * string_length);

    if (new_card->name == NULL) {
        uart0_puts_p(PSTR("fail: new_card->name"));
        return;
    }

    strcpy(new_card->name, argv[2]);
    //add null terminator
    new_card->name[string_length - 1] = '\0';
    new_card->next = head;
    head = new_card;
    //print card info
    uart0_puts_p(PSTR("Card added."));
    uart0_puts(CRLF);
    uart0_puts_p(PSTR("Card UID: "));

    for (byte i = 0; i < head->size; i++) {
        print_byte_as_ascii_hex(head->uid[i]);
    }

    uart0_puts_p(PSTR(" ("));
    char buffer[4] = {0x00};
    itoa(head->size, buffer, 10);
    uart0_puts(buffer);
    uart0_puts_p(PSTR(" bytes)"));
    uart0_puts(CRLF);
    uart0_puts_p(PSTR("Holder name: "));
    uart0_puts(head->name);
    uart0_puts(CRLF);
}


void cli_print_cards(const char *const *argv)
{
    (void) argv;
    card *cursor = head;
    char buffer[4] = {0x00};
    char count_buf[4] = {0x00};
    int count = 1;

    if (cursor == NULL) {
        uart0_puts_p(PSTR("Card list is empty."));
        uart0_puts(CRLF);
    }

    while (cursor != NULL) {
        itoa(count, count_buf, 10);
        count_buf[4] = '\0';
        uart0_puts(count_buf);
        uart0_puts_p(PSTR(". UID["));
        itoa(cursor->size, buffer, 10);
        buffer[4] = '\0';
        uart0_puts(buffer);
        uart0_puts_p(PSTR("]: "));

        for (byte i = 0; i < cursor->size; i++) {
            print_byte_as_ascii_hex(cursor->uid[i]);
        }

        uart0_puts_p(PSTR(" holder name: "));
        uart0_puts(cursor->name);
        uart0_puts(CRLF);
        count++;
        cursor = cursor->next;
    }
}

void cli_remove_card(const char *const *argv)
{
    size_t target_size = strlen(argv[1]) / 2;
    uint8_t *target_uid = (uint8_t *)malloc(target_size * sizeof(uint8_t));

    if (target_uid == NULL) {
        uart0_puts_p(PSTR("malloc fail: target_uid"));
        return;
    }

    tallymarker_hextobin(argv[1], target_uid, target_size);
    card *cursor = head;
    card *prev = NULL;

    while (cursor != NULL) {
        if (cursor->size == target_size) {
            int match = memcmp(cursor->uid, target_uid, target_size);

            if (match == 0) {
                //TODO separate method ?
                if (prev == NULL) {
                    if (cursor->next == NULL) {
                        free(cursor->uid);
                        free(cursor->name);
                        free(cursor);
                        head = NULL;
                    } else {
                        head = cursor->next;
                        free(cursor->uid);
                        free(cursor->name);
                        free(cursor);
                    }
                } else if (cursor == head) {
                    prev->next = NULL;
                    free(cursor->uid);
                    free(cursor->name);
                    free(cursor);
                } else {
                    prev->next = cursor->next;
                    cursor->uid = NULL;
                    cursor->name = NULL;
                    free(cursor->uid);
                    free(cursor->name);
                    free(cursor);
                }

                uart0_puts_p(PSTR("Removed card UID: "));

                for (size_t i = 0; i < target_size; i++) {
                    print_byte_as_ascii_hex(target_uid[i]);
                }

                uart0_puts(CRLF);
                return;
            }
        }

        prev = cursor;
        cursor = cursor->next;
    }

    uart0_puts_p(PSTR("Card not found!"));
    uart0_puts(CRLF);
    free(target_uid);
}

void cli_mem_stat(const char *const *argv)
{
    (void) argv;
    char print_buf[256] = {0x00};
    extern int __heap_start, *__brkval;
    int v;
    int space;
    static int prev_space;
    space = (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval);
    uart0_puts_p(PSTR("Heap statistics\r\n"));
    sprintf_P(print_buf, PSTR("Used: %u\r\nFree: %u\r\n"), getMemoryUsed(),
              getFreeMemory());
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nSpace between stack and heap:\r\n"));
    sprintf_P(print_buf, PSTR("Current  %d\r\nPrevious %d\r\nChange   %d\r\n"),
              space, prev_space, space - prev_space);
    uart0_puts(print_buf);
    uart0_puts_p(PSTR("\r\nFreelist\r\n"));
    sprintf_P(print_buf, PSTR("Freelist size:             %u\r\n"),
              getFreeListSize());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Blocks in freelist:        %u\r\n"),
              getNumberOfBlocksInFreeList());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest block in freelist: %u\r\n"),
              getLargestBlockInFreeList());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest freelist block:    %u\r\n"),
              getLargestAvailableMemoryBlock());
    uart0_puts(print_buf);
    sprintf_P(print_buf, PSTR("Largest allocable block:   %u\r\n"),
              getLargestNonFreeListBlock());
    uart0_puts(print_buf);
    prev_space = space;
}



void cli_print_cmd_error(void)
{
    uart0_puts_p(PSTR("Command not implemented.\r\n\tUse <help> to get help.\r\n"));
}


void cli_print_cmd_arg_error(void)
{
    uart0_puts_p(
        PSTR("To few or too many arguments for this command\r\n\tUse <help>\r\n"));
}


int cli_execute(int argc, const char *const *argv)
{
    // Move cursor to new line. Then user can see what was entered.
    //FIXME Why microrl does not do it?
    uart0_puts_p(PSTR("\r\n"));

    for (uint8_t i = 0; i < NUM_ELEMS(cli_cmds); i++) {
        if (!strcmp_P(argv[0], cli_cmds[i].cmd)) {
            // Test do we have correct arguments to run command
            // Function arguments count shall be defined in struct
            if ((argc - 1) != cli_cmds[i].func_argc) {
                cli_print_cmd_arg_error();
                return 0;
            }

            // Hand argv over to function via function pointer,
            // cross fingers and hope that funcion handles it properly
            cli_cmds[i].func_p (argv);
            return 0;
        }
    }

    cli_print_cmd_error();
    return 0;
}

