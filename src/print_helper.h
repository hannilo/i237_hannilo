#ifndef PRINT_HELPER_H
#define PRINT_HELPER_H

void print_ascii_tbl(void);
void print_for_human(const unsigned char *array, const size_t len);
void print_byte_as_ascii_hex(const uint8_t b);
#ifdef DEBUG
void debug_byte_as_ascii_hex(const uint8_t b);
#endif /* DEBUG */
void to_upper(char str[]);
void tallymarker_hextobin(const char * str, uint8_t * bytes, size_t blen);

#endif /* PRINT_HELPER_H */
