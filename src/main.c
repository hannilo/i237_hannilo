#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/io.h>
#include <time.h>
#include <string.h>

#include "../lib/hd44780_111/hd44780.h"
#include "../lib/helius_microrl/microrl.h"
#include "../lib/andygock_avr-uart/uart.h"
#include "../lib/matejx_avr_lib/mfrc522.h"
#include "../lib/andy_brown_memdebug/memdebug.h"

#include "debug.h"
#include "cli_microrl.h"
#include "hmi_msg.h"
#include "print_helper.h"

#define UART_BAUD        9600
#define UART_STATUS_MASK 0x00FF
#define LED_RED          PORTA0 // Arduino Mega digital pin 22
#define LED_GRN          PORTA2 // Arduino Mega digital pin 24
#define LED_BLU          PORTA4 // Arduino Mega digital pin 26
#define LED_YLW          PORTB7 // Onboard yellow led

//4 ticks is one second
#define SECONDS_1        4
#define SECONDS_3        12

//Create microrl object and pointer on it
microrl_t rl;
microrl_t *prl = &rl;

volatile time_t now;
volatile time_t door_open_time;
volatile time_t display_update_time;
volatile time_t tick;

typedef enum {
    door_opening,
    door_open,
    door_closing,
    door_closed
} door_state_t;

typedef enum {
    display_clear,
    display_name,
    display_invalid,
    display_no_update
} display_state_t;

extern struct card *head;
door_state_t door_state;
display_state_t display_state;
card *user;
char lcd_buf[16] = {0x00};


static inline void init_sys_timer(void)
{
    TCCR1A = 0;
    TCCR1B = 0;
    TCCR1B |= _BV(WGM12); // Turn on CTC (Clear Timer on Compare)
    TCCR1B |= (_BV(CS11) | _BV(CS10)); // fCPU/64
    OCR1A = 62549; // Note that it is actually two registers OCR5AH and OCR5AL
    TIMSK1 |= _BV(OCIE1A); // Output Compare A Match Interrupt Enable
}


static inline void init_uart1(void)
{
    uart1_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart1_puts(VER_FW);
    uart1_puts(VER_LIBC);
}


static inline void init_uart0(void)
{
    uart0_init(UART_BAUD_SELECT(UART_BAUD, F_CPU));
    uart0_puts(NAME);
    uart0_puts(CRLF);
    uart0_puts_p((PSTR("Use backspace to delete entry and enter to confirm.")));
    uart0_puts(CRLF);
    uart0_puts_p((PSTR("Arrow keys and DEL are not functional.")));
    uart0_puts(CRLF);
    //init with callback to print function
    microrl_init(prl, uart0_puts);
    //callback - pointer to function that is called when user
    //presses enter
    microrl_set_execute_callback(prl, cli_execute);
}


static inline void init_leds(void)
{
    DDRA |= _BV(LED_RED);
    PORTA &= ~_BV(LED_RED);
    DDRA |= _BV(LED_GRN);
    PORTA &= ~_BV(LED_GRN);
    DDRA |= _BV(LED_BLU);
    PORTA &= ~_BV(LED_BLU);
#ifdef DEBUG
    DDRB |= _BV(LED_YLW);
    PORTB &= ~_BV(LED_YLW);
#endif /* DEBUG */
}


static inline void init_rfid_reader(void)
{
    MFRC522_init();
    PCD_Init();
}


static inline void heartbeat(void)
{
    static time_t prev_time;
    //buffer for holding runtime
    char ascii_buf[11] = {0x00};
    uint8_t tmp = now / 4;

    //one tick is .25s
    if ((now - prev_time) >= SECONDS_1) {
        ltoa(tmp, ascii_buf, 10);
        uart1_puts_p(PSTR("Uptime: "));
        uart1_puts(ascii_buf);
        uart1_puts_p(PSTR(" s"));
        uart1_puts(CRLF);
        PORTA ^= _BV(LED_BLU);
        prev_time = now;
    }
}


static inline void door_unlock(void)
{
    door_state = door_open;
#ifdef DEBUG
    uart1_puts_p(PSTR("open"));
    uart1_puts(CRLF);
#endif /* DEBUG */
    PORTA |= _BV(LED_GRN);
    PORTA &= ~_BV(LED_RED);
    door_open_time = time(NULL);
}


static inline void door_lock(void)
{
    door_state = door_closed;
#ifdef DEBUG
    uart1_puts_p(PSTR("closed"));
    uart1_puts(CRLF);
#endif /* DEBUG */
    PORTA &= ~_BV(LED_GRN);
    PORTA |= _BV(LED_RED);
}


static inline void validate_card(void)
{
    Uid uid;
    Uid *uid_ptr = &uid;
    PICC_ReadCardSerial(uid_ptr);
#ifdef DEBUG
    char buf[3] = {0x00};
    uart1_puts_p(PSTR("Card UID: "));

    for (byte i = 0; i < uid.size; i++) {
        debug_byte_as_ascii_hex(uid.uidByte[i]);
    }

    uart1_puts_p(PSTR(" ("));
    itoa(uid.size, buf, 10);
    uart1_puts(buf);
    uart1_puts_p(PSTR(" bytes)"));
    uart1_puts(CRLF);
#endif /* DEBUG */
    card *c = head;

    while (c != NULL) {
#ifdef DEBUG
        uart1_puts_p(PSTR("debug: "));

        for (byte i = 0; i < uid.size; i++) {
            debug_byte_as_ascii_hex(uid.uidByte[i]);
        }

        uart1_puts_p(PSTR(" v "));

        for (byte i = 0; i < c->size; i++) {
            debug_byte_as_ascii_hex(c->uid[i]);
        }

        uart1_puts(CRLF);
#endif /* DEBUG */
        int match = memcmp(c->uid, uid.uidByte, uid.size);

        if (match == 0) {
            user = c;
            door_state = door_opening;
            display_state = display_name;
            return;
        }

        c = c->next;
    }

    //card is invalid
    user = NULL;
    door_state = door_closing;
    display_state = display_invalid;
}


static inline void handle_door(void)
{
    switch (door_state) {
    case door_opening:
        door_open_time = time(NULL);
        door_unlock();
        door_state = door_open;
        break;

    case door_open:
        if (now - door_open_time >= SECONDS_3) {
            door_state = door_closing;
        }

        break;

    case door_closing:
        door_lock();
        door_state = door_closed;
        break;

    case door_closed:
        break;
    }
}


static inline void lcd_clear(void)
{
    lcd_clr(LCD_ROW_2_START, LCD_VISIBLE_COLS);
    lcd_goto(LCD_ROW_2_START);
}


static inline void handle_display()
{
    switch (display_state) {
    case display_clear:
        if ((now - display_update_time) >= SECONDS_3) {
            lcd_clear();
            display_state = display_no_update;
        }

        break;

    case display_name:
        lcd_clear();
        strncpy(lcd_buf, user->name, LCD_VISIBLE_COLS);
        lcd_puts(lcd_buf);
        display_update_time = now;
        display_state = display_clear;
        break;

    case display_invalid:
        lcd_clear();
        lcd_puts_P(PSTR("Invalid card"));
        display_update_time = now;
        display_state = display_clear;
        break;

    case display_no_update:
        break;
    }
}


static inline void scan_card(void)
{
    now = time(NULL);

    //.25 seconds
    if ((now - tick) >= 1) {
        tick = now;
        PORTB ^= _BV(LED_YLW);

        if (PICC_IsNewCardPresent()) {
            validate_card();
        }

        handle_door();
        handle_display();
    }
}


void main(void)
{
    init_uart0();
    init_uart1();
    init_leds();
    init_sys_timer();
    lcd_init();
    lcd_clrscr();
    lcd_home();
    lcd_puts(NAME);
    init_rfid_reader();
    door_lock();
    door_open_time = time(NULL);
    display_update_time = time(NULL);
    door_state = door_closed;
    display_state = display_clear;
    sei();

    while (1) {
        heartbeat();
        //microrl_set_echo(1);
        microrl_insert_char(prl, (uart0_getc() & UART_STATUS_MASK));
        //handle_door();
        scan_card();
    }
}


ISR(TIMER1_COMPA_vect)
{
    system_tick();
}
