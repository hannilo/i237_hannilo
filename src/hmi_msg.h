#ifndef HMI_MSG_H
#define HMI_MSG_H

#define VER_FW   "Version: " FW_VERSION " built on: " __DATE__ " " __TIME__ "\r\n"
#define VER_LIBC "avr-libc version: " __AVR_LIBC_VERSION_STRING__ " avr-gcc version: " __VERSION__ "\r\n"

extern const char NAME[];
extern const char CRLF[];
extern const char INIT_CONS[];
extern PGM_P const NUMBERS[];

#endif /* HMI_MSG_H */
